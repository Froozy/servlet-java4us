package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Make that form</h2>" +
				"<form action='data' method = 'POST'>" +
				"Firstname*: <input type='text' name='firstname' required/> <br />" +
				"Surname*: <input type='text' name='surname' required/> <br />" +
				"Email*: <input type='text' name='email' required/> <br />" +
				"Repeat email*: <input type='text' name='remail' required/> <br />" +
				"Your employer*: <input type='text' name='job' /> <br />" +
				"<input type='checkbox' name='from' value='Work'>Work<br />" +
				"<input type='checkbox' name='from' value='School'>School<br />" +
				"<input type='checkbox' name='from' value='Fb'>Facebook<br />" +
				"<input type='checkbox' name='from' value='Friends'>Friends<br />" +
				"<input type='checkbox' name='from' value='Another'>Another<br />" +
				"What you do?*<input type='textarea' rows='30' cols='40' name='coment'/> <br />" +
				"<input type='submit' value='Register' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}
