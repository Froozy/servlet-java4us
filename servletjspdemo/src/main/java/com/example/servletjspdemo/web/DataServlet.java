package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.servletjspdemo.domain.Person;
import com.example.servletjspdemo.service.StorageService;

@WebServlet(urlPatterns = "/data")
public class DataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private HttpSession session;
	boolean full,mail,again;

	
	
	public Person MakePerson(HttpServletRequest request) {

		Person person = new Person();

			String selectedOption = "";
			for (String from : request.getParameterValues("from")) {
				selectedOption += from + ", ";
			}
			person.setFirstname(request.getParameter("firstname"));
			person.setSurname(request.getParameter("surname"));
			person.setEmail(request.getParameter("email"));
			person.setRemail(request.getParameter("remail"));
			person.setEmployer(request.getParameter("employer"));
			person.setInfo_from(request.getParameter(selectedOption));
			person.setWork(request.getParameter("work"));
		return person;
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if(full == false && mail == false && again == false)
		{
			again = true;
			request.getRequestDispatcher("Comm.jsp").forward(request, response);
		}
		if(full == true)
		{
			request.getRequestDispatcher("CommFull.jsp").forward(request, response);
		}
		if(mail == true)
		{
			request.getRequestDispatcher("CommEmail.jsp").forward(request, response);
		}
		if(again == true)
		{
			request.getRequestDispatcher("CommAgain.jsp").forward(request, response);
		}
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		full = false;
		mail = false;
		Person person = MakePerson(request);
		if (person.getEmail().equals(person.getRemail())){
					if (StorageService.db.size() < 5) {
						session = request.getSession(true);
						StorageService.add(person);
					}
					else{
						full = true;
					}
		}
		else{
			mail = true;
		}
		doGet(request, response);
	}

}
