package com.example.servletjspdemo.domain;

public class Person {
	
	private String firstname = "unknown";
	private String surname = "unknown";
	private String email;
	private String remail;
	private String employer = "unknown";
	private String info_from;
	private String work = "unknown";
	
	public Person() {
		super();
	}
	
	public Person(String firstname,String surname, String email,String remail,String employer,String info_from,String work) {
		super();
		this.firstname = firstname;
		this.surname = surname;
		this.email = email;
		this.remail = remail;
		this.employer = employer;
		this.info_from = info_from;
		this.work = work;
	}

	public String getRemail() {
		return remail;
	}

	public void setRemail(String remail) {
		this.remail = remail;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getSurname() {
		return surname;
	}

	public String getEmail() {
		return email;
	}

	public String getEmployer() {
		return employer;
	}

	public String getInfo_from() {
		return info_from;
	}

	public String getWork() {
		return work;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public void setInfo_from(String info_from) {
		this.info_from = info_from;
	}

	public void setWork(String work) {
		this.work = work;
	}

}
